# Usage

What I do is cloning this repo and just make some soft links:

```sh
git clone https://gitlab.com/mikelgarai/conf.git conf
ln -s conf/vim/vimrc ~/.vimrc
ln -s conf/vim/gvimrc ~/.gvimrc
ln -s conf/vim/dot_vim ~/.vim
```

Or you can just execute the script: 
```sh
conf/vim/install.sh
```

And thats it, then if you want you can add local stuff without
having local changes with the following files:

* ~/.vimrc\_local
  * File sourced directly at the end of .vimrc
* ~/.vimrc\_local\_plug
  * Config sourced in .vimrc between Plug#begin and Plug#end
* ~/.gvimrc\_local
  * File sourced in .gvimrc


