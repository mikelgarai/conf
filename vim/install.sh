#!/bin/bash

SCRIPTDIR_REL=`dirname $0`
SCRIPTDIR=`readlink -f $SCRIPTDIR_REL`


if test -e ~/.vimrc -o -e ~/.gvimrc -o -e ~/.vim \
	    -o -h ~/.vimrc -o -h ~/.gvimrc -o -h ~/.vim ; then
	RESP="KK"
	while test $RESP != "n" -a $RESP != "y"; do
		echo -n "Files ~/.vimrc ~/.gvimrc or directory ~/.vim already exist, do you want me to remove them? (y/n)"
		read -n 1 RESP
		echo
	done
	if test $RESP != "y"; then
		exit 1
	fi
	rm -rf ~/.vimrc ~/.gvimrc ~/.vim
fi

ln -s $SCRIPTDIR/vimrc ~/.vimrc
ln -s $SCRIPTDIR/gvimrc ~/.gvimrc
ln -s $SCRIPTDIR/dot_vim ~/.vim
