" Vim syntax file
" Language:	PRM MAC log file
" Maintainer:	Mikel Garai
" Version:	0.1
" Last Change:	2016-03-02

if exists("b:current_syntax")
  finish
endif

syn case match

syn keyword prm_mac_keyword	  CON_REQ_B CON_REQ_S CON_CLS_B CON_CLS_S
syn keyword prm_mac_keyword	  MUL_JOIN_B MUL_JOIN_S MUL_LEAVE_B MUL_LEAVE_S
syn keyword prm_mac_keyword	  REG_REQ REG_RSP REG_ACK REG_REJ REG_UNR_S REG_UNR_B
syn keyword prm_mac_keyword	  PRO_REQ_B PRO_REQ_S PRO_ACK PRO_REJ
syn keyword prm_mac_keyword	  PRO_DEM_B PRO_DEM_S
syn keyword prm_mac_keyword	  FRA BSI_IND BSI_ACK ALV_B ALV_S
syn keyword prm_mac_keyword	  DATA TX
syn keyword prm_mac_keyword2  Beacon GPDU  Promotion Needed

syn keyword prm_mac_field arq bcn caps channels cnt delta dev nextgroup=prm_mac_field_point
syn keyword prm_mac_field dncost evm frametime frq lcid level nextgroup=prm_mac_field_point
syn keyword prm_mac_field lnid nsid pna rxcnt rxpow seq sid slt sna snr spc ssid time nextgroup=prm_mac_field_point
syn keyword prm_mac_field txcnt typ type upcost  nextgroup=prm_mac_field_point
syn keyword prm_mac_field_space eui48 nextgroup=prm_mac_field_point_space

syn match prm_mac_field_point ":" nextgroup=prm_mac_value
syn match prm_mac_field_point_space ": " nextgroup=prm_mac_value

syn match prm_mac_invalid "(Invalid CRC.*)" 
syn match prm_mac_invalid "Invalid packet length" 
syn match prm_mac_invalid "SYNC ERROR" 

syn keyword prm_mac_type RX DO UP

syn match prm_mac_value  '\<\(phy\|plc\|udp\|all\)[0-9]*\>' contained display
syn match prm_mac_value  '\<[0-9\.a-fA-F,:ynx]\+\>' contained display
syn match prm_mac_value  '+\<[0-9\.,]\+\>' contained display
syn match prm_mac_value_date  display "\<\d\d\d\d\(-\|/\)\d\d\(-\|/\)\d\d \d\d:\d\d:\d\d\>" nextgroup=prm_mac_value_time
syn match prm_mac_value_time  ' \+\<[0-9\.]\+\>' contained display nextgroup=prm_mac_value_duration1

syn match prm_mac_value_duration1 "(" contained display nextgroup=prm_mac_value
syn match prm_mac_value  display "\<d[b8q]psk\(_f\|\)\>" nextgroup=prm_mac_value_time

syn match prm_mac_units "(Vrms)\|(dB)" 

syn match prm_mac_pdu_hex  ' \+[0-9a-f]0 |[0-9a-f \[\]]\+\>' 

"syn region  processingPar	transparent start="[" end="]" contains=TOP,processingParErr


"hi def link prm_mac_field       String
hi def link prm_mac_units       Comment
hi def link prm_mac_type        Function
hi def link prm_mac_value       Constant
hi def link prm_mac_value_time  Constant
hi def link prm_mac_value_date  String
hi def link prm_mac_keyword     Keyword
hi def link prm_mac_keyword2    Function
hi def link prm_mac_pdu_hex     Comment
hi def link prm_mac_invalid     Error
"hi def link example Character
"hi def link example Comment
"hi def link example Conditional
"hi def link example Constant
"hi def link example Error
"hi def link example Exception
"hi def link example Float
"hi def link example Function
"hi def link example Label
"hi def link example Number
"hi def link example Operator
"hi def link example PreProc
"hi def link example Repeat
"hi def link example SpecialChar
"hi def link example StorageClass
"hi def link example String
"hi def link example Todo
"hi def link example Type

let b:current_syntax = "prm_mac"

