// ==UserScript==
// @name     ziv_dorlet_exit_time_calculator
// @version  0.4.2
// @include  http://eszmd035/DorletCliente*
// @require  http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js
// @grant    none
// ==/UserScript==

var last_val = "";
var $ = jQuery.noConflict(true)
var written = false
var today;
var interval = null;

function parseMinutes(str){
  var tmp = str.split(":");
  return parseInt(tmp[0])*60 + parseInt(tmp[1]);
}

// dont get me started about this... 
function minsToStr(mins){
  return ("0"+Math.floor(mins/60)).slice(-2)+":"+("0"+(mins%60)).slice(-2);
}

$( document ).ready(function() {
    ziv_init()
});


function ziv_init() {
  var today_obj = $("#MenuPersonal1_lblPresente");
  
  if (today_obj.length < 1)
    return;
  today = today_obj[0].innerText;
  
  var start = today.substring(9);
  today = today.substring(0,8);
  // start == "hh:mm" today == "dd/mm/yy"
  var start_mins = parseMinutes(start);
  var pringado_mins = 0;
  if (start_mins < 8*60){
    pringado_mins = 8*60 - start_mins;
    start_mins = 8*60;
  }
  
  if ($("#btnLanzar").length == 1){
      $( "#btnLanzar" ).on( "click", function() {
  			if (interval == null){
        	interval = setInterval(findMorningTime, 1000);
        }
      });
  }
  var morning = (start_mins / 60 < 13);
  var friday = (new Date().getDay() == 5);
  var morning_mins = 0;
  var enter_time = start_mins;
  if (!morning){
    var morning_mins_day = localStorage.getItem("ziv_hours_today");
    if (morning_mins_day == today){
	    morning_mins = parseInt(localStorage.getItem("ziv_hours_mins"));
      enter_time = parseInt(localStorage.getItem("ziv_hours_entertime"));
      if (enter_time < 8*60){
        pringado_mins = 8*60 - enter_time
        morning_mins -= pringado_mins;
        enter_time = 8*60;
      }
    } else {
      if (location.href.search("DorletCliente/personal/PrincipalPersonal.aspx") > 0){
				window.location.href = "/DorletCliente/personal/infoPersonal.aspx";
  		} else if (location.href.search("personal/infoPersonal.aspx") > 0){
        $("#btnLanzar")[0].click();
        if (interval == null)
          interval = setInterval(findMorningTime, 1000);
      }
      return;
    }
  }
  var today_mins = 8*60+15;
  
  if (friday)
    today_mins = 5*60 + 30;
  else if (morning)
    // la comida
    today_mins += 45;
  else
    // las horas de la mañana
    today_mins -= morning_mins;

  var color = "lightgreen";
  if (!friday && morning){
    color = "yellow";
  }
	start_mins += today_mins;
  if (!friday && start_mins < enter_time + 9*60){
    pringado_mins += enter_time + 9*60 - start_mins;
    start_mins = enter_time + 9*60;
  }
  if ($("#salida_text").length == 0) {
  	$("#MenuPersonal1_lblPresente").after(' <span style="font-weight: bolder;">Salida:</span> <span id="salida_text" style="color:'+color+'">'+ minsToStr(start_mins) + "</span>");
  } else {
    var val = $("#salida_text");
    val[0].innerText = minsToStr(start_mins); 
    val.css("color", color);
  }
  if ($("#pringado_text").length == 0) {
  	$("#salida_text").after(' <span id="pringado_text" style="color:#ffc0c0;"></span>');
  } 
  var val = $("#pringado_text")[0];
  if (pringado_mins == 0){
    val.innerHTML = "";
  } else {
    val.innerHTML = '('+minsToStr(start_mins-pringado_mins) + ')'; 
  }

}
	  
	

function findMorningTime(){
  var table = $("#gvFichajes");
  var cur_val = table[0].innerHTML;
 	if (last_val == cur_val){
    return;
  }
  last_val = cur_val;
  
  var rows = table.find("tr");
  
  for(var i = 0; i < rows.length; i++){
    if (rows[i].children.length == 7 && rows[i].children[0].nodeName == "TD"){
      var date_tmp = rows[i].children[0].innerText;
      /* dirty dirty way to change 4 digit year to 2 digit, stops working on 2030 */
      date_tmp = date_tmp.replace("/201", "/1").replace("/202", "2");
      if (date_tmp == today){
        var enter_time = parseMinutes(rows[i].children[1].innerText);
        var mins = parseMinutes(rows[i].children[6].innerText);
        localStorage.setItem("ziv_hours_mins", mins);
        localStorage.setItem("ziv_hours_entertime", enter_time);
        localStorage.setItem("ziv_hours_today", today);
        ziv_init();
        if (interval != null)
       		clearInterval(interval);
        return;
      }
  	}
  }
}
