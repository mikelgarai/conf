#!/bin/bash

if test $# -lt 1 ; then
	echo "arguments: the files to be sent (directories not supported"
	exit 1
fi
while test z"$1" != z ; do
	TMP=`dirname "$1"`
	if test z"$path" != z; then
		path="$path,"
	fi
	if test "${TMP:0:1}" != "/" ; then
		path="${path}$PWD/$1"
	else
		path="${path}$1"
	fi
	filename=`echo $1 | sed 's:.*/\([^/]*\):\1:'`
	if test z"$subject" = z; then
		subject="$filename"
	else
		subject="$subject, $filename"
	fi
	shift
done
echo "icedove -compose \"attachment='${path}',subject='$subject'\""
icedove -compose "attachment='${path}',subject='$subject'"
