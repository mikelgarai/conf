#!/bin/bash

#first="true"
#for proc in `top -b -n 1 | head -n 10 | tail -n 3 | cut -c61-71`; do
#	if test $first != "true"; then
#		printf "|"
#	fi
#	first="false"
#	printf "%10s" $(printf "%.10s" ${proc})
#done

NUM=3
if test $# -gt 0; then
	NUM=$1
fi

top -b -n 1 | head -n $((7 + $NUM)) | tail -n $NUM | sed "s/  */ /g" | sed "s/^ *//"| cut -d ' ' -f9,12 | tr ' ' ':' | while read line; do 
	printf "%-20s | " ${line:0:20}
done
