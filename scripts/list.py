#!/usr/bin/python

import sys

def print_usage():
	print "usage:"
	print "   ",sys.argv[0], "<file-a> <operator> <file-b>"
	print "where operator can be:"
	print "  -          : print file-a's lines that are not in file-b's lines"
	print "  and        : print file-a's lines that are in file-b's lines"
	print "  and2       : print file-a's lines that are in file-b's lines, "
	print "               printing file-b's comments instead of files-a's"
	print "  xor        : print lines from both files that are only in one of them"
	print "\nNOTE: to be compared lines are cleaned in the following way:"
	print "  The character \"#\" and the rest until newline arrives are cut"
	print "  The result is transformed to lowercase"
	print "  Then it is stripped (blank characaters in the begining and end cut) "
	exit(1)

def clean_pk(line):
	try:
		commentpos = line.find("#")
		if (commentpos != -1):
			line = line[:commentpos]
		return line.strip().lower()
	except:
		return ""
	
if (len(sys.argv) != 4):
	print_usage();

file1 = open(sys.argv[1], "r");
file2 = open(sys.argv[3], "r");

list1source = file1.readlines();
list2source = file2.readlines();

file1.close();
file2.close();

list1 = []
for l in list1source:
	list1.append([clean_pk(l), l])
list2 = []
for l in list2source:
	list2.append([clean_pk(l), l])

if (sys.argv[2] == "and"):
	for line1 in list1:
		for line2 in list2:
			if (line1[0] == line2[0]):
				print line1[1],
				break;
elif (sys.argv[2] == "and2"):
	for line1 in list1:
		for line2 in list2:
			if (line1[0] == line2[0]):
				print line2[1],
				break;
elif (sys.argv[2] == "-"):
	for line1 in list1:
		contained = False
		for line2 in list2:
			if (line2[0] == line1[0]):
				contained = True;
		if (not contained):
			print line1[1],
elif (sys.argv[2] == "xor"):
	for line1 in list1:
		contained = False
		for line2 in list2:
			if (line2[0] == line1[0]):
				contained = True;
		if (not contained):
			print line1[1],
	for line2 in list2:
		contained = False
		for line1 in list1:
			if (line2[0] == line1[0]):
				contained = True;
		if (not contained):
			print line2[1],
else:
	print_usage();

