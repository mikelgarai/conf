#!/bin/bash

if test ! -d jpg
then
	mkdir jpg
fi

CORES=4

function wait_num() {
	while test `jobs -p | wc -l` -gt $1; do
		sleep .1
	done
}
i=1
for f in $@
do 
	wait_num $CORES
	fto=`echo $f | sed 's/^\(.*\)\.\([^.]*\)/\1.jpg/'`
	if test -e jpg/$fto
	then
		echo "$i/$# jpg/$fto already exists"
	else
		echo "$i/$# converting: " $f
		convert $f -quality 97 jpg/$fto &
	fi
	i=`expr $i + 1`
done
wait_num 1
