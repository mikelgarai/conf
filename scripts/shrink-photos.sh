#!/bin/bash

if test ! -d small
then
	mkdir small
fi


CORES=4

function wait_num() {
	while test `jobs -p | wc -l` -gt $1; do
		sleep .1
	done
}

i=1
for f in $@
do 
	wait_num $CORES
	fto=`echo $f | sed 's/^\(.*\)\.\([^.]*\)/\1.jpg/'`
	if test -e small/$fto
	then
		echo "$i/$# small/$fto already exists"
	else
		echo "$i/$# converting: " $f
		convert $f -resize 2000x2000 small/$fto &
	fi
	i=`expr $i + 1`
done

wait_num 1
