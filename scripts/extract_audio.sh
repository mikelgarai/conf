#!/bin/sh

if test "$#" = "1"; then
	echo "Audio info: "
	ffprove $1 2>&1 | grep Audio
elif test "$#" = "2"; then
	# ffmpeg -i "$1" -f mp3 -ab 320000 -vn "$2"
	ffmpeg -i "$1" -acodec copy -vn "$2"
else
	echo "usage: $0 <invideo> [<outaudio>]"
	echo "The audio will not be reencoded"
	exit 1
fi

