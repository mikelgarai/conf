#!/usr/bin/python
import os
import glob
import subprocess

def runBash(cmd):
    p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
    out = p.stdout.read().decode('ascii').strip()
    print (out)
    return out  #This is the stdout from the shell command

files = runBash("ls -rt")
files = files.split("\n")
i = 0
for file in files:
	os.rename(file, "%(a)07d_%(b)s" % {"a":i , "b":file})
	#print file , " ---> " , "%(a)07d_%(b)s" % {"a":i , "b":file}
	i = i + 1
