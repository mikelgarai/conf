#!/usr/bin/python3

from IPython import embed

import sys
import os

pyutilspath = os.environ["HOME"]+"/pyutils"
if (os.path.isdir(pyutilspath)):
	sys.path.append(pyutilspath)
	if (os.path.isfile(pyutilspath + "/utils.py")):
		from utils import *

embed(using=False)
