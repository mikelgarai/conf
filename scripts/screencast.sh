#!/bin/bash



file=screencast_`date +%Y%m%d_%H%M%S_%3N`.mkv
while test -f $file; do
	file=screencast_`date +%Y%m%d_%H%M%S_%3N`.mkv
done

xwininfo -root > $file.tmp

ROOT_WIDTH=$(cat $file.tmp | grep "^ *Width:" | sed "s/.* \([0-9]*\)/\1/")
ROOT_HEIGHT=$(cat $file.tmp | grep "^ *Height:" | sed "s/.* \([0-9]*\)/\1/")
RESOLUTION="${ROOT_WIDTH}x${ROOT_HEIGHT}"
rm $file.tmp

SCALE=n
WIN=n
for ARG in $@; do
	if test $ARG = "--scale"; then
		SCALE=y
	elif test $ARG = "--win"; then
		WIN=y
	else
		echo "usage:"
		echo "  $0 [--scale] [--win]"
		exit 1
	fi
done

get_scaled_param(){
	if test $SCALE = "y"; then
		echo "-vf scale=$(expr $1 / 2)x$(expr $2 / 2)"
	fi
}

if test $WIN = "n" ; then
	# fullscreen 
	
	ffmpeg -f alsa -i default -acodec pcm_s16le -f x11grab -s $RESOLUTION \
		-r 25 -i $DISPLAY -qscale 0 $(get_scaled_param $ROOT_WIDTH $ROOT_HEIGHT) $file

else
	# windowed 
	
	echo "Choose the window you want to capture (you can't move it afterwards)"
	
	xwininfo > $file.tmp
	
	
	WIDTH=$(cat $file.tmp | grep "^ *Width:" | \
	        sed "s/.* \([0-9]*\)/\1/")
	HEIGHT=$(cat $file.tmp | grep "^ *Height:" | \
	         sed "s/.* \([0-9]*\)/\1/")
	LEFT=$(cat $file.tmp | grep "Absolute upper-left X:" | \
	       sed "s/.* \([0-9]*\)/\1/")
	TOP=$(cat $file.tmp | grep "Absolute upper-left Y:" | \
	      sed "s/.* \([0-9]*\)/\1/")
	
	WIDTH=$(expr $WIDTH / 4 "*" 4)
	HEIGHT=$(expr $HEIGHT / 4 "*" 4)
	rm $file.tmp
	
	echo calc: $WIDTH x $HEIGHT , $LEFT - $TOP
	
	
	
	# for audio (with pulseaudio) add the following to the command line:
	ffmpeg -f alsa -i default -acodec pcm_s16le -f x11grab \
	       -s ${WIDTH}x${HEIGHT} \
		   -r 25 -i ${DISPLAY}+${LEFT},${TOP} -qscale 0 $(get_scaled_param $WIDTH $HEIGHT) $file
fi
