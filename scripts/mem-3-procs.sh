#!/bin/bash

ps axo pmem,comm | grep -v COMMAND | sort -rn | head -n3 | sed "s/^ *//" | tr ' ' ':' | while read line; do 
	printf "%-15s | " ${line:0:15}
done
