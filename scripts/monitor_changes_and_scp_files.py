#!/usr/bin/python3

import subprocess
import sys
import os
import time

PROMPT="MIK_PROMPT_some_ugly_string"

def read_until_prompt(f):
	ret = []
	line = f.readline().strip()
	#print(line)
	while line != PROMPT:
		ret.append(line.strip())
		line = f.readline().strip()
		#print(line)
	return ret

host = sys.argv[1]
params = sys.argv[2:]
print("ssh " + host)
ssh = subprocess.Popen(['ssh', host],
                       stdin=subprocess.PIPE, 
                       stdout=subprocess.PIPE,
                       universal_newlines=True,
                       bufsize=0)
patterns = " ".join(params)
last_res = ""

ssh.stdin.write("echo "+PROMPT+"\n")
# just discard until PS has the correct value
read_until_prompt(ssh.stdout)

while True:
	#print("md5sum "+patterns+" ; echo " + PROMPT + "\n")
	ssh.stdin.write("md5sum "+patterns+" ; echo " + PROMPT + "\n")
	
	res = "\n".join(read_until_prompt(ssh.stdout))
	if res != last_res:
		last_res = res
		#print("scp " + sys.argv[1] + ":" + (sys.argv[1] + ":").join(params) + " .")
		os.system("scp " + sys.argv[1] + ":" + (sys.argv[1] + ":").join(params) + " .")
	time.sleep(1)
