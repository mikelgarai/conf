import urllib.parse
import sys
import os

def launch(prog, argv):                         
	type = argv[0]
	if len(argv) > 1:
		commandline = " ".join(argv[1:])
		commandline = urllib.parse.quote(commandline)
	else:
		commandline = ""
	if type == "g":
		commandline = "http://www.google.com/search?q=" + commandline
	elif type == "gi":
		commandline = "http://images.google.com/images?q=" + commandline
	elif type == "du":
		commandline = "https://duckduckgo.com/?q=" + commandline
	elif type == "dui":
		commandline = "https://duckduckgo.com/?ia=images&q=" + commandline
	elif type == "duv":
		commandline = "https://duckduckgo.com/?ia=videos&q=" + commandline
	elif type == "gk":
		commandline = "http://keep.google.com/"
	elif type == "y":
		if commandline == "":
			commandline = "http://www.youtube.com/"
		else:
			commandline = "http://www.youtube.com/results?search_query=" + commandline
	elif type == "gm":
		commandline = "http://maps.google.com/?q=" + commandline
	elif type == "gt":
		commandline = "https://translate.google.com/?q=" + commandline
	elif type == "imdb":
		commandline = "http://www.imdb.com/find?s=all&q=" + commandline
	elif type == "es":
		commandline = "http://www.wordreference.com/es/translation.asp?tranword=" + commandline + "&dict=enes"
	elif type == "se":
		commandline = "http://www.wordreference.com/es/translation.asp?tranword=" + commandline + "&dict=esen"
	elif type == "rae":
		commandline = "http://rae.es/"+commandline
	elif type == "ud":
		commandline = "http://www.urbandictionary.com/define.php?term=" + commandline
	elif type == "d":
		commandline = "https://duckduckgo.com/?q=" + commandline
	elif type == "n":
		commandline = "https://enno.dict.cc/?s=" + commandline
	elif commandline != "":
		commandline = "http://www.google.com/search?q=" + type + "%20" + commandline
	else:
		commandline = type
	print (prog+" \""+commandline+"\"")
	os.system(prog+" \""+commandline+"\"")
