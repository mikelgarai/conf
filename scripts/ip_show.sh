#!/bin/sh

ip a | grep "inet " | grep -v 127 | sed "s/\s*inet \([^ ]*\) .*/\1/"
