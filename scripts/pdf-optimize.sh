#!/bin/sh

if test $# -ne 1; then
	echo "usage: "
	echo "$0 <inputfile>"
	exit 1
fi

src=$1
dst=`echo $1 | sed "s/\.pdf$/.optimized.pdf/"`

if $src = $dst; then
	echo "The parameter must be a PDF"
	exit 1
fi

gs -dNOPAUSE -dBATCH -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/screen -sOutputFile="$dst" "$src"
