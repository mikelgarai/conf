#!/usr/bin/python3

# This script is a downloader for XKCD comics
#
# It has only been tested in linux, and probably will not work on 
# windows, but if you are using windows you probably don't know
# what XKCD is so is alright
#
# ----- NOTE  ----------------
# this downloads a random comic from XKCD every time is run, so be respectful
# with Randall and don't use it in a loop unless you add a "sleep", because
# randall was nice enough to provide a json API, so don't abuse it.
#
# My initial idea is to request one comic every 1-10 minutes or so and use them
# as screensaver, this script will check to avoid downloading the same one, so
# after a while all the comics will have been downloaded
# ----------------------------
#


import urllib.request
import random
import json
from PIL import Image
from PIL import ImageFont
from PIL import ImageOps
from PIL import ImageDraw
import glob
import os
import time
import sys

xkcd_dir = os.getenv("HOME") + "/xkcd"
if (not os.path.exists(xkcd_dir)):
	os.mkdir(xkcd_dir)
xkcd_dir += "/"

def draw_text(fon, lines, width, img = False, y = 0):
	ret = 0
	dr = False
	if (img):
		dr = ImageDraw.Draw(img)
	for line in lines:
		words = line.split(" ")
		while (len(words) > 0):
			cur_word = 2
			while (fon.getsize(" ".join(words[0:cur_word]))[0] < width - 2 and cur_word <= len(words)):
				cur_word += 1
			cur_word -= 1
			if (dr):
				dr.text((2, y + ret), " ".join(words[0:cur_word]), font=fon, fill="#909090")
			ret += fon.getsize(" ".join(words[0:cur_word]))[1] + 2
			words = words[cur_word:]
	return ret

def check_last():
	global xkcd_dir
	fname = xkcd_dir + "last_num.txt"
	if os.path.exists(fname):
		f = open(fname, "r")
		lines = f.readlines()
		f.close()
		if len(lines) >= 2 and abs(int(lines[0].strip()) - time.time()) < 86400:
			return int(lines[1].strip())
		os.remove(fname)
	f = open(fname, "w")
	f.write(str(int(time.time())) + "\n")
	res = urllib.request.urlopen('https://xkcd.com/info.0.json')
	txt = res.read().decode('utf-8')
	dic = json.loads(txt, strict=False)
	f.write(str(dic["num"]) + "\n")
	f.close()
	return dic["num"]

last_comic = check_last()
a = -1
retries = 1000
not_images = [1608, 1663, 404]
while a == -1 or a in not_images or len(glob.glob(xkcd_dir + str(a) +'_*.??g')) != 0 or len(glob.glob(xkcd_dir + str(a) +'_*.gif')) != 0:
	a = random.randint(1, last_comic)
	retries -= 1
	if retries <= 0:
		print ("Couldn't find a not downloaded one, giving up..")
		sys.exit(1)

print ("downloading %d (retries %d / total %d)" % (a, 999 - retries, len(glob.glob(xkcd_dir + '*_*.??g'))))

scriptpath=os.path.dirname(os.path.abspath(__file__))
fon = ImageFont.truetype(scriptpath+"/xkcd.ttf", 21, encoding="utf-8")
url = 'https://xkcd.com/' + str(a) + '/info.0.json' 

res = urllib.request.urlopen(url)
txt = res.read().decode('utf-8')
dic = json.loads(txt, strict=False)


localname = str(a) + '_' + dic["img"][dic["img"].rfind("/")+1:]

urllib.request.urlretrieve(dic["img"], "tmp_"+localname)

img = Image.open("tmp_"+localname)


lines = dic["alt"].split('\n')

height = draw_text(fon, lines, img.width)

img2 = Image.new("RGB", (img.width, img.height + height + 8), color = "white")
img2.paste(img)
dr = ImageDraw.Draw(img2)
dr.line([(0, img.height + 2), (img.width, img.height + 2)], fill="black", width=2)
draw_text(fon, lines, img.width, img2, img.height + 8)

os.remove("tmp_" + localname)
img2.save(xkcd_dir+localname)


