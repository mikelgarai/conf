#!/usr/bin/python

import pygtk

pygtk.require('2.0')

import gtk
import os
import sys
import stat
import time


class tray_icon_t:


	def __init__(self, path, secs):

		self.statusIcon = gtk.StatusIcon()
		self.statusIcon.set_from_stock(gtk.STOCK_NO)
		self.statusIcon.set_visible(True)
		self.statusIcon.connect('activate', self.click_cb, self.statusIcon)

		self.menu = gtk.Menu()
		self.path = path
		self.ms = secs * 1000

		self.menuItem = gtk.ImageMenuItem(gtk.STOCK_QUIT)
		self.menuItem.connect('activate', self.quit_cb, self.statusIcon)
		self.menu.append(self.menuItem)

		self.statusIcon.connect('popup-menu', self.popup_menu_cb, self.menu)
		self.statusIcon.set_visible(1)
		gtk.timeout_add(self.ms, self.timeout)

		self.last_time = 0;
		self.update_last_change()
		self.updated = False
		self.update_tray_icon()

		gtk.main()


	def quit_cb(self, widget, data = None):
		gtk.main_quit()

	def click_cb(self, widget, data = None):
		self.statusIcon.set_from_stock(gtk.STOCK_NO)


	def popup_menu_cb(self, widget, button, time, data = None):
		if button == 3:
			if data:
				data.show_all()
				data.popup(None, None, gtk.status_icon_position_menu,
				           3, time, self.statusIcon)
	def update_tray_icon(self):
		s = time.strftime("%Y:%m:%d %H:%M:%S", 
		                  time.localtime(self.last_time))
		self.statusIcon.set_tooltip(s + "\n" + self.path)
	def get_last_time(self, path):
		try:
			s = os.stat(path)
			ret = s.st_mtime
			if (stat.S_ISDIR(s.st_mode)):
				for f in os.listdir(path):
					tmp = self.get_last_time(path+"/"+f)
					if (tmp > ret):
						ret = tmp
			return ret
		except:
			return 0
	def update_last_change(self):
		new_time = self.get_last_time(self.path)
		
		if (self.last_time != new_time):
			self.updated = True
		self.last_time = new_time

	def timeout(self, *args):
		self.update_last_change()
		if (self.updated):
			self.statusIcon.set_from_stock(gtk.STOCK_YES)
			self.update_tray_icon()
			self.updated = False
		gtk.timeout_add(self.ms, self.timeout)


if __name__ == "__main__":
	if (len(sys.argv) != 2 and len(sys.argv) != 3):
		sys.stderr.write("Usage: %s <path> [<time (s)>]\n" 
		                 % (sys.argv[0]));
		sys.exit(1)
	secs = 3
	if (len(sys.argv) == 3):
		secs = int(sys.argv[2])
	tray_icon = tray_icon_t(sys.argv[1], secs)
