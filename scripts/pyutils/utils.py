#!/usr/bin/python3

import datetime
import time
import math
import sys
import os
import re

from IPython import get_ipython


class hr():
	def __init__(self, h = 0, m = 0, s = 0):
		self.s = h*3600 + m*60 + s
	def __add__(self, b):
		return hr(s = self.s + b.s)
	def __sub__(self, b):
		return hr(s = self.s - b.s)
	def __truediv__(self, num):
		return hr(s = self.s / num)
	def __mul__(self, num):
		return hr(s = self.s * num)
	def __str__(self):
		return "%02d:%02d:%02d" % (self.s//3600, self.s//60 % 60, self.s % 60)
	def __repr__(self):
		return self.__str__()

def reg_char(val, ch, num):
	if(len(val) < num):
		return val
	return reg_char(val[:-num], ch, num) + ch + val[-num:]

def mode_comb():
	formatter = get_ipython().display_formatter.formatters['text/plain']
	formatter.for_type(int, lambda n, p, cycle: p.text("0x%x %d %s" % (n, n, reg_char(bin(n), ",", 4))))
	
def mode_hex():
	formatter = get_ipython().display_formatter.formatters['text/plain']
	formatter.for_type(int, lambda n, p, cycle: p.text("0x%x" % n))

def mode_dec():
	formatter = get_ipython().display_formatter.formatters['text/plain']
	formatter.for_type(int, lambda n, p, cycle: p.text("%d" % n))

def mode_oct():
	formatter = get_ipython().display_formatter.formatters['text/plain']
	formatter.for_type(int, lambda n, p, cycle: p.text("%s" % (oct(n))))

def mode_bin():
	formatter = get_ipython().display_formatter.formatters['text/plain']
	formatter.for_type(int, lambda n, p, cycle: p.text("%s" % (bin(n))))

mode_comb()
