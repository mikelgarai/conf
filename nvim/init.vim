syntax on 				"sets the syntax highlight
set nocompatible
set cc=121
set hidden 				"enables the hidden buffers by default
set ttyfast
set ignorecase 			"to ignore case in searchs
set hlsearch 			"to highlight the search results
set incsearch 			"incremental search
set number 				"makes the line numbers visible
set scrolloff=3 		"Keep 3 lines above and below cursor
set sidescrolloff=3 	"Keep 3 columns left/right the cursor
"set mouse=a				" mouse support in all modes
set mousehide			" hide the mouse when typing text
"displays a line in the cursor
set cursorline
set wildmenu
set backspace=indent,start,eol

let mapleader = ","

augroup column
	autocmd!
	autocmd ColorScheme * hi ColorColumn ctermbg=236 guibg=#505055
augroup END

fun! BracketAlign()
	let l:line = getline(".")
	let l:pos = col(".")-1
	let l:spaces = len(matchstr(strpart(l:line, l:pos), "^ *"))
	let l:pos += l:spaces
	let l:op_pos = strridx(strpart(l:line, 0, l:pos), "(")
	if l:op_pos < 0
		let l:op_pos = len(matchstr(l:line, "^[ \t]*"))-1
	endif
	let l:nextline = substitute(strpart(l:line, 0, l:op_pos+1), "[^\t]", " ", "g").strpart(l:line, l:pos)
	let l:line = strpart(l:line, 0, l:pos)
	call setline(".", l:line)
	call append(line("."), l:nextline)
endfunction
" q: sucks
nmap q: :q

nmap Y y$
"to avoid errors when the vim is splitted, in insert mode C-W was a problem
imap <C-w> <ESC><C-w>

"set copyindent 				"to copy the indentation of the last line
"set autoindent smartindent	" turn on auto/smart indenting
"set cinkeys=0{,0},!^F,o,O,e " disable the # indenting to the first line
set undolevels=1000             " number of forgivable mistakes

" ,p and shift-insert will paste the X buffer, even on the command line
map <F8> <ESC>:make<CR>
map <A-]> :let _my_f=expand("<cword>")<CR><C-w><C-w>:exec("tag "._my_f)<CR><C-w>p

" indent from the cursor position to the next line where this line
" has a parenthesis
" nnoremap <leader>a i80i k0f(jldw
nnoremap <leader>a :call BracketAlign()<CR>
" replace the tailing ";" by {} in the next 2 lines
nnoremap <leader>i f;xo{}
" add a c style comment to be used as title
" let @t='O/* 63a-a */35hi  i'
nnoremap <leader>t 0d$i/* 72a-a */010li  P072ldwi 

nnoremap <leader>h ggO/"%pF/ld0VgU:.!sed "s/[^A-Z0-9]/_/g"i#ifndef A_INCLUDEDyyplcwdefineyyGplcwendif /* A */

nnoremap <leader>c DOp==Vgq

" This is to avoid using css so copy&paste into libreoffice works properly
let g:html_use_css = 0

"  Autodownload vim-plug if it's not installed. Neat!
if empty(glob('~/.config/nvim/autoload/plug.vim'))
	silent !mkdir -p ~/.config/nvim/autoload
	silent !curl -fLo ~/.config/nvim/autoload/plug.vim
				\ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
	autocmd VimEnter * PlugInstall
endif

if !has('gui_running')
  set t_Co=256
endif

call plug#begin('~/.config/nvim/plugged')
Plug 'junegunn/vim-easy-align'

Plug 'vim-scripts/closetag.vim'

Plug 'christoomey/vim-tmux-navigator'

Plug 'ton/vim-alternate'

Plug 'tikhomirov/vim-glsl'

if !empty(glob('~/.vimrc_local_plug'))
	source ~/.vimrc_local_plug
endif

Plug 'https://gitlab.com/mikelgarai/vim_hl.git'


Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'

Plug 'tpope/vim-fugitive'

Plug 'neovim/nvim-lspconfig'
Plug 'hrsh7th/cmp-nvim-lsp'
Plug 'hrsh7th/cmp-nvim-lua'
Plug 'hrsh7th/cmp-buffer'
Plug 'hrsh7th/cmp-path'
Plug 'hrsh7th/cmp-cmdline'
Plug 'hrsh7th/cmp-calc'
Plug 'hrsh7th/nvim-cmp'
Plug 'f3fora/cmp-spell'
Plug 'ray-x/lsp_signature.nvim'
Plug 'sbdchd/neoformat'
Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}
call plug#end()


" :A for :Alternate
command! A Alternate

" terminal navigation
tnoremap <C-h> <C-\><C-n>:TmuxNavigateLeft<cr>
tnoremap <C-j> <C-\><C-n>:TmuxNavigateUp<cr>
tnoremap <C-k> <C-\><C-n>:TmuxNavigateDown<cr>
tnoremap <C-l> <C-\><C-n>:TmuxNavigateRight<cr>

set noexpandtab 		"do not expand tabulators
set tabstop=4 			"tab with to 4
set sw=4

set grepprg=grep\ -n\ -R\ --exclude=*.o,*.obj,*~,*.pyc,tags\ --exclude-dir=.ccls-cache\ --exclude-dir=.git\ $*

set fo=croq
fun! Alupdate() 
	if !exists('b:alshow_match')
		let b:alshow_match=-1
	endif
	if !exists('b:alshow_enabled')
		let b:alshow_enabled = 0
	end
	if b:alshow_enabled == 1
		highlight StartTab ctermbg=236 guibg=#505055
		if b:alshow_match == -1
			let b:alshow_match = matchadd("StartTab", '^\t\+')
		end
	else
		if b:alshow_match != -1
			call matchdelete(b:alshow_match)
			let b:alshow_match = -1
		end
	end
endfunction

fun! Alshow() 
	let b:alshow_enabled  = 1
	call Alupdate()
endfunction 

fun! Alhide() 
	let b:alshow_enabled  = 0
	call Alupdate()
endfunction 

au BufEnter * call Alupdate()

command! Alshow call Alshow()
command! Alhide call Alhide()
Alhide
"include settings for source code files
autocmd FileType c,cpp,cc,h,hh,hpp,python,javascript,php,java	source ~/.config/nvim/c.vim

" Avoid the fucking horrible python PEP sucking thing of 
" fucking spaces that the fucking VIM started using from some
" stupid reason
autocmd FileType python setlocal tabstop=4
autocmd FileType python setlocal noexpandtab
autocmd FileType python setlocal sw=4

set laststatus=2
set nobackup
set nowritebackup
set noswapfile

fun! MusicTranslate() range
	let r={'DO':' C', 'RE':' D', 'MI':' E', 'FA':' F', 'SOL':'  G', 'LA':' A', 'SI':' B'}
	let ran='%'
	if a:firstline != a:lastline
		let ran=a:firstline.','.a:lastline
	endif
	execute ran.':s/\<\(DO\|RE\|MI\|FA\|SOL\|LA\|SI\)/\=r[submatch(1)]/gI'
		
endfunction

function! CpHtml() range
	if a:firstline == a:lastline
		execute ':TOhtml'
	else
		execute a:firstline.','.a:lastline.':TOhtml'
	endif
	execute "%!xclip -selection clipboard -t text/html"
	execute "bd!"
endfunction

command! -range CpHtml <line1>,<line2>call CpHtml()

function! SvnBlame(...)
	let cur_line = line(".")
	if a:0 >= 1
		execute ":%!svn blame -r".a:1." %"
	else
		execute ":%!svn blame %"
	endif
	execute ":normal ".cur_line."G"
endfunction

" hi CursorLine ctermbg=darkgray cterm=none guibg=#333355
" hi Cursor guibg=#666688

if !empty(glob('~/.vimrc_local'))
	source ~/.vimrc_local
endif

map <leader>gf :e <cfile><cr>

let curdir = getcwd()
let maxdirs = 10
while maxdirs > 0 && curdir != "" && curdir != "/" && curdir != $HOME && isdirectory(curdir)
	if !empty(glob(curdir . "/.vimrc"))
		execute "source " . curdir . "/.vimrc"
	endif
	let curdir = fnamemodify(curdir, ":h")
	let maxdirs = maxdirs - 1
endwhile

nnoremap <leader>* :HL \<<c-r>=expand("<cword>")<cr>\><cr>

nnoremap <leader>c "+
vnoremap <leader>c "+

" copy to buffer
vnoremap ,y :w! ~/.vimbuffer<CR>
nnoremap ,y :.w! ~/.vimbuffer<CR>
" " paste from buffer
noremap ,p :r ~/.vimbuffer<CR>

" Make ':b' command alwasy be ':b *' to avoid the default behavior of 
" prioritizing prefix search over matching
cnoremap <expr> b<Space> getcmdline() == '' ? 'b *' : 'b '

set path=$PWD/**

" UGLY, super UGLY workaround to solve gx not working sometimes
" Not sure if its a XFCE or netrw issue, but is screwed up, got
" the workaround from here and changed it for my own scripts:
"
" https://github.com/vim/vim/issues/4738
"
nnoremap gx yiW:!b <cWORD><CR> <C-r>" & <CR><CR>

nnoremap <leader>f :GFiles<CR>

set signcolumn=yes

set statusline=%f\ %h%w%m%r\ %{get(b:,'coc_current_function','')}%=%(%l,%c%V\ %=\ %P%)

fun! DiffColors() 
	hi DiffAdd      ctermbg=darkgreen gui=none    guifg=NONE          guibg=#004320
	hi DiffChange   ctermbg=black gui=none    guifg=NONE          guibg=#00172b
	hi DiffDelete   ctermfg=red ctermbg=52 gui=none    guifg=#d40000       guibg=#640000
	"hi DiffDelete   ctermfg=red ctermbg=darkred gui=none    guifg=#d40000       guibg=#640000
	hi DiffText     ctermbg=darkblue gui=none    guifg=NONE          guibg=#003e6b
endfunction 


command! DiffColors call DiffColors()

lua require("init")
